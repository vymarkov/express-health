// require('dotenv').config()

const express = require('express')
const { checkHealth, whitelist } = require('../health')
const {
  checks: {
    postgres: checkPostgres,
    aws: checkMinioServer
  }
} = require('../health')

const config = {
  postgresUrl: process.env.POSTGRES_URL || 'postgres://postgres:postgres@localhost:5432/postgres',
  redisUrl: 'redis://localhost:6379/0',
  s3: {
    endpoint: process.env.AWS_S3_ENDPOINT || 'http://0.0.0.0:9100/barfoo',
    accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID || 'admin',
    secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY || 'password',
    s3BucketEndpoint: true,
    signatureVersion: 'v4',
    bucketName: process.env.AWS_S3_BUCKET || 'barfoo'
  }
}

const app = express()

app.get('/health', checkHealth(['127.0.0.1'], {
  pg: checkPostgres(config.postgresUrl),
  minio: checkMinioServer(config.s3)
}))

app.listen(process.env.PORT || 3001, () => {
  console.log('Example app listening on port 3001!')
})