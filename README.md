### WARNING!
Package is not ready for production usage, work is still in progress.

### Installation
```bash
$ npm install gitlab:vymarkov/express-health
```

### Examples

#### Express.js
```javascript
const express = require('express')
const { checkHealth, whitelist } = require('express-health')
const {
  checks: {
    postgres: checkPostgres,
    aws: checkMinioServer
  }
} = require('express-health')

const config = {
  postgresUrl: process.env.POSTGRES_URL || 'postgres://postgres:postgres@localhost:5432/postgres',
  s3: {
    endpoint: process.env.AWS_S3_ENDPOINT || 'http://0.0.0.0:9100/barfoo',
    accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID || 'admin',
    secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY || 'password',
    s3BucketEndpoint: true,
    signatureVersion: 'v4',
    bucketName: process.env.AWS_S3_BUCKET || 'barfoo'
  }
}

const app = express()
/**
  for the following ips list server responds with full info about health checks 
  which contain info about service and message with shout info about error
  otherwise server respond with short info about service status (UP or DOWN) generally
 */
const whitelistIps = ['127.0.0.1']
app.get('/health', checkHealth(whitelistIps, {
  pg: checkPostgres(config.postgresUrl),
  minio: checkMinioServer(config.s3)
}))
app.listen(process.env.PORT || 3001, () => {
  console.log('Example app listening on port 3001!')
})
```

Also you can mount health app as a sub express.js app to you target application.  Firstrly, you need add config into root project. That config should exports two required things - a whitelist of `ips` and a list of `heath checks`.
That config will be used by sub application and should be called `health.config.js`. 
The example of config below: 
```javascript
const { checkHealth, checks: { aws: checkAWSS3Server } } = require('express-health')
const config = {
  s3: {
    endpoint: process.env.AWS_S3_ENDPOINT || 'http://0.0.0.0:9100/vtng.app',
    accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID || 'admin',
    secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY || 'password',
    s3BucketEndpoint: true,
    signatureVersion: 'v4',
    bucketName: process.env.AWS_S3_BUCKET || 'vtng.app'
  }
}

module.exports = {
  ips: ['127.0.0.1'],
  checks: {
    minio: checkAWSS3Server(config.s3)
  }
}
```
Mount health sub application:
```javascript
const app = express()
const health = require('express-health/lib/express')
app.use('/health', health)
app.listen(process.env.PORT || 3001, () => {
  console.log('Example app listening on port 3001!')
})
```

Fetch service status info:
```bash
$ http -v http://127.0.0.1:3001/health
```