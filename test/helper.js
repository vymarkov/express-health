process.env.NODE_ENV = 'test'

process.env.MINIO_HOST = process.env.MINIO_HOST || '0.0.0.0'
process.env.MINIO_PORT = process.env.MINIO_PORT || 9000

process.env.AWS_S3_BUCKET = process.env.AWS_S3_BUCKET || 'barfoo'
process.env.AWS_S3_ENDPOINT = process.env.AWS_S3_ENDPOINT || `http://${process.env.MINIO_HOST}:${process.env.MINIO_PORT}/${process.env.AWS_S3_BUCKET}"`
process.env.AWS_S3_ACCESS_KEY_ID = process.env.AWS_S3_ACCESS_KEY_ID || 'admin'
process.env.AWS_S3_SECRET_ACCESS_KEY = process.env.AWS_S3_SECRET_ACCESS_KEY || 'password'

const util = require('util')

const express = require('express')
const Minio = require('minio')
const Docker = require('dockerode')
const docker = new Docker({
  socketPath: '/var/run/docker.sock',
  Promise
})

const s3Options = Object.freeze({
  endpoint: process.env.AWS_S3_ENDPOINT,
  accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_S3_SECRET_ACCESS_KEY,
  s3BucketEndpoint: true,
  signatureVersion: 'v4',
  bucketName: process.env.AWS_S3_BUCKET,
  region: 'eu-central-1'
})

const minioOptions = {
  endPoint: process.env.MINIO_HOST,
  port: +process.env.MINIO_PORT,
  secure: false,
  accessKey: s3Options.accessKeyId,
  secretKey: s3Options.secretAccessKey
}

const minioClient = new Minio.Client(minioOptions)
minioClient.makeBucket = util.promisify(minioClient.makeBucket)
minioClient._bucketExists = minioClient.bucketExists
minioClient.bucketExists = (bucketName) => {
  return new Promise((resolve, reject) => {
    return minioClient._bucketExists(bucketName, (err) => {
      if (err && err.code === 'NoSuchBucket') {
        return resolve(false)
      } else if (err) {
        return reject(err)
      }
      return resolve(true)
    })
  })
}

async function createBucket() {
  return minioClient.bucketExists(s3Options.bucketName)
    .then(exists => {
      if (!exists) {
        return minioClient.makeBucket(s3Options.bucketName, s3Options.region)
      }
    })
}

async function runServer(checkHealthMW) {
  const app = express()
  app.get('/health', checkHealthMW)
  return app
}

module.exports = {
  runServer,
  minioClient,
  minioOptions,
  createBucket,
  s3Options,
  docker
}