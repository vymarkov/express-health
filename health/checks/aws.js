const AWS = require('aws-sdk')
const S3FS = require('s3fs')
const uuid = require('uuid').v4

const { STATUS } = require('../config')

module.exports = checkAWS

function checkAWS(s3Options) {
  return () => {
    return new Promise((resolve, reject) => {
      const fs = new S3FS(s3Options.bucketName, s3Options)
      const filename = `${uuid()}.txt`

      fs.writeFile(filename, 'hello, world!', err => {
        if (err) {
          return resolve({
            status: STATUS.DOWN,
            message: err.message
          })
        }

        fs.unlink(filename, err => {
          if (err) {
            return resolve({
              status: STATUS.DOWN,
              message: err.message
            })
          }
          return resolve({ status: STATUS.UP })
        })
      })
    })
  }
}