const util = require('util')
const redis = require('redis')

const { STATUS } = require('../config')

module.exports = checkRedis

function checkRedis(redisUrl) {
  return new Promise((resolve, reject) => {
    if (util.isNullOrUndefined(redisUrl)) {
      throw new Error('redisUrl is missing')
    }

    const c = redis.createClient(redisUrl)
    c.on('ready', () => {
      return resolve({ status: STATUS.UP })
    })
    c.on('error', err => {
      if (err) {
        return resolve({
          status: STATUS.DOWN,
          message: err.message
        })
      } else {
        resolve({ status: STATUS.UP })
      }
    })
  })
}