module.exports = {
  postgres: require('./postgres'),
  redis: require('./redis'),
  smtp: require('./smtp'),
  aws: require('./aws')
}
