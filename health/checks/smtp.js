const util = require('util')
const url = require('url')
const SMTPConnection = require('nodemailer/lib/smtp-connection')

const { STATUS } = require('../config')

module.exports = checkSMTPServer

function checkSMTPServer(config) {
  return () => {
    return new Promise(resolve => {
      let creds = {}
      if (util.isString(config)) {
        const connString = url.parse(config)
        connString.query = {}
        connString.query.connectionTimeout = 3000
        const { user, pass } = (connString.auth || '').split(':')

        creds = Object.assign({
          port: connString.port,
          host: connString.hostname,
          connectionTimeout: connString.query.connectionTimeout,
          user, pass
        }, connString.query)
      } else {
        config.connectionTimeout = config.connectionTimeout || 5000
        creds = Object.assign({}, config)
      }

      const connection = new SMTPConnection(creds)
      connection.on('error', err => {
        return resolve({
          status: STATUS.DOWN,
          message: err.message
        })
      })
      connection.connect(err => {
        if (err) {
          return resolve({
            status: STATUS.DOWN,
            message: err.message
          })
        }
        resolve({ status: STATUS.UP })
      })
    })
  }
}
