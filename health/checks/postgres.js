const pg = require('pg')
const util = require('util')

const { STATUS } = require('../config')

module.exports = checkPostgres

function checkPostgres(pgUrl) {
  if (util.isNullOrUndefined(pgUrl)) {
    throw new Error('pgUrl is missing')
  }
  return () => {
    return new Promise((resolve, reject) => {
      const client = new pg.Client(pgUrl)
      client.connect(err => {
        if (err) {
          return resolve({
            status: STATUS.DOWN,
            message: err.message
          })
        }
        client.query('SELECT NOW()', (err, rows) => {
          if (err) {
            return resolve({
              status: STATUS.DOWN,
              message: err.message
            })
          }
          resolve({ status: STATUS.UP })
        })
      })
    })
  }
}